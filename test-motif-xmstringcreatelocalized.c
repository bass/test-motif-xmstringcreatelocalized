/*
 * $Id$
 */
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <wchar.h>
#include <errno.h>

#include <locale.h>
#include <langinfo.h>

#include <iconv.h>

#include <assert.h>

#include <Xm/XmAll.h>

const char *keyBackground = "#dcdad5";

const char *valueBackground = "#ffffff";

#if defined(__sun) && defined(__SVR4)
/*
 * In Solaris, sizeof(wchar_t) is normally 4.
 *
 * Both UCS-2 and UCS-4 can be converted to UTF-8,
 * while UTF-16 can only be converted to UCS-4 (see iconv_unicode(5)),
 * so UCS-2 is preferred to UTF-16.
 *
 * "WCHAR_T" is a sane default which will be rejected by Solaris' iconv_open(),
 * but still is a valid input for iconv_open() from GNU libiconv.
 */
#define ICONV_WCHAR_T sizeof(wchar_t) == 4 ? "UCS-4" : sizeof(wchar_t) == 2 ? "UCS-2" : "WCHAR_T"
#else
#define ICONV_WCHAR_T "WCHAR_T"
#endif

#if defined(__sun) && defined(__SVR4)
#define PRINTF_SIZE_T "%u"
#else
#define PRINTF_SIZE_T "%zu"
#endif

char *iconvToNewA(const char * const source, const char *sourceCharset, const char *targetCharset);

char *iconvToNewW(const wchar_t * const source, const char *targetCharset);

/*
 * UTF-8 may use up to 6 bytes to encode a char.
 * For Cyrillic, 2 bytes is enough though.
 */
const unsigned int bytesPerChar = 2;

void addLine(Widget parent,
		char *keyWidgetName,
		char *keyLabel,
		char *valueWidgetName,
		const XmString valueLabel);

Pixel toColor(const Widget widget, const char *name) {
	XrmValue from;
	from.addr = (char *) name;
	from.size = (unsigned int) strlen(name) + 1;

	XrmValue to;
	to.addr = NULL;

	XtConvertAndStore(widget, XmRString, &from, XmRPixel, &to);

	if (to.addr != NULL) {
		return *((Pixel *) to.addr);
	}

	return XmUNSPECIFIED_PIXEL;
}

int main(int argc, char *argv[]) {
	const char *locale = setlocale(LC_ALL, "");
	if (locale == NULL) {
		fprintf(stderr, "Locale unavailable.\n");
	}

	char *localeCharset = nl_langinfo(CODESET);
	printf("locale: %s\n", locale);
	printf("run-time charset: %s\n", localeCharset);

	XtAppContext appContext;

	XtSetLanguageProc(NULL, (XtLanguageProc) NULL, (XtPointer) NULL);

	const char *applicationClass = "XmRowColumnTest";
	String fallbackResources[] = {
			"*.XmLabel.fontList: 		-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-*-*, \
							-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-*-*, \
							-monotype-arial-medium-r-normal--*-90-*-*-p-*-*-*, \
			                                -*-*-medium-r-normal--*-90-*-*-p-*-*-*:",
			"*.labelAnsi.fontList:		-monotype-arial-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-*-*-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251:",
			"*.labelAnsi.foreground:	red",
			"*.labelIso.fontList:		-monotype-arial-medium-r-normal--*-90-*-*-p-*-iso8859-5, "
							"-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-iso8859-5, "
							"-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-iso8859-5, "
							"-*-*-medium-r-normal--*-90-*-*-p-*-iso8859-5:",
			"*.labelIso.foreground:		blue",
			"*.labelKoi.fontList:		-monotype-arial-medium-r-normal--*-90-*-*-p-*-koi8-r, "
							"-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-koi8-r, "
							"-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-koi8-r, "
							"-*-*-medium-r-normal--*-90-*-*-p-*-koi8-r:",
			"*.labelKoi.foreground:		#008000",
			"*.labelDos.fontList:		-monotype-arial-medium-r-normal--*-90-*-*-p-*-ibm-cp866, "
							"-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-ibm-cp866, "
							"-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-ibm-cp866, "
							"-*-*-medium-r-normal--*-90-*-*-p-*-ibm-cp866:",
			"*.labelDos.foreground:		magenta",
			"*.labelUnicode.fontList:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-iso10646-1, \
							-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-iso10646-1, \
							-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-iso10646-1, \
							-misc-fixed-medium-r-normal--*-90-*-*-c-*-iso10646-1, \
							-*-*-medium-r-normal--*-90-*-*-p-*-iso10646-1:",
			"*.labelUnicode.foreground:	green",
			NULL,
	};
	const Widget topLevel = XtVaAppInitialize(&appContext, applicationClass, NULL, 0, &argc, argv, fallbackResources, NULL);
	XtVaSetValues(topLevel, XmNtitle, "XmRowColumn Test, " XmVERSION_STRING, NULL);
	XtVaSetValues(topLevel, XmNiconName, "XmRowColumn Test, " XmVERSION_STRING " (Icon)", NULL);

	const Widget mainWindow = XmCreateMainWindow(topLevel, "mainWindow", NULL, 0);
	XtManageChild(mainWindow);

	const Cardinal rowColumnPropertyCount = 1;
	Arg rowColumnProperties[rowColumnPropertyCount];
	Cardinal i = 0;
	/*
	 * Since XtSetArg is a macro, it's impossible to use i++ inline: the
	 * count will be incremented twice.
	 */
	XtSetArg(rowColumnProperties[i], XmNrowColumnType, XmWORK_AREA); i++;
	assert(i == rowColumnPropertyCount);

	const Widget rowColumn = XmCreateRowColumn(mainWindow, "rowColumn", rowColumnProperties, rowColumnPropertyCount);
	XtManageChild(rowColumn);

	XtVaSetValues(rowColumn, XmNpacking, XmPACK_COLUMN, NULL);
	XtVaSetValues(rowColumn, XmNorientation, XmHORIZONTAL, NULL);
	const char *rowColumnBackground = "#dcdad5";
	XtVaSetValues(rowColumn, XmNbackground, toColor(rowColumn, rowColumnBackground), NULL);

	const wchar_t * const wcs = L"\u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042b\u042c\u042d\u042e\u042f\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044b\u044c\u044d\u044e\u044f";

	char *ansiLabelClass = "labelAnsi";
	char *isoLabelClass = "labelIso";
	char *koiLabelClass = "labelKoi";
	char *dosLabelClass = "labelDos";
	char *unicodeLabelClass = "labelUnicode";

	int columnCount = 0;

	char * const stringAnsi = iconvToNewW(wcs, "CP1251");
	{
		const XmString labelString1 = XmStringCreateLocalized(stringAnsi);
		addLine(rowColumn, "key1", "XmStringCreateLocalized(CP1251):", ansiLabelClass, labelString1);
		XmStringFree(labelString1);
		columnCount++;
	}
	free(stringAnsi);

	char * const stringIso = iconvToNewW(wcs, "ISO-8859-5");
	{
		const XmString labelString1 = XmStringCreateLocalized(stringIso);
		addLine(rowColumn, "key1", "XmStringCreateLocalized(ISO-8859-5):", isoLabelClass, labelString1);
		XmStringFree(labelString1);
		columnCount++;
	}
	free(stringIso);

	char * const stringKoi = iconvToNewW(wcs, "KOI8-R");
	{
		const XmString labelString1 = XmStringCreateLocalized(stringKoi);
		addLine(rowColumn, "key1", "XmStringCreateLocalized(KOI8-R):", koiLabelClass, labelString1);
		XmStringFree(labelString1);
		columnCount++;
	}
	free(stringKoi);

	char * const stringDos = iconvToNewW(wcs, "CP866");
	{
		const XmString labelString1 = XmStringCreateLocalized(stringDos);
		addLine(rowColumn, "key1", "XmStringCreateLocalized(CP866):", dosLabelClass, labelString1);
		XmStringFree(labelString1);
		columnCount++;
	}
	free(stringDos);

	/*
	 * This may or may not succeed, depending on client locale and/or
	 * X server version. For some client/server combinations, *.UTF-8 locale
	 * is required while some other clients behave well even in C locale.
	 * For some other servers, even setting client locale to *.UTF-8 is not
	 * sufficient.
	 */
	char * const mbs = iconvToNewW(wcs, "UTF-8");
	if (mbs != NULL && strcasecmp(localeCharset, "UTF-8") == 0) {
		const XmString labelString1 = XmStringCreateLocalized(mbs);
		addLine(rowColumn, "key1", "XmStringCreateLocalized(multi-byte string):", unicodeLabelClass, labelString1);
		XmStringFree(labelString1);
		columnCount++;
	}
	free(mbs);

	XtVaSetValues(rowColumn, XmNnumColumns, columnCount, NULL); // For XmHORIZONTAL orientation, this is actually a number of rows

	XtRealizeWidget(topLevel);
	XtAppMainLoop(appContext);

	return 0;
}

void addLine(Widget parent,
		char *keyWidgetName,
		char *keyLabel,
		char *valueWidgetName,
		const XmString valueLabel) {
	const Widget label0 = XmCreateLabel(parent, keyWidgetName, NULL, 0);
	XtManageChild(label0);
	const XmString labelString0 = XmStringCreate(keyLabel, XmSTRING_DEFAULT_CHARSET);
	XtVaSetValues(label0, XmNlabelString, labelString0, NULL);
	XtVaSetValues(label0, XtVaTypedArg, XmNbackground, XmRString, keyBackground, strlen(keyBackground) + 1, NULL);
	XmStringFree(labelString0);


	const Widget label1 = XmCreateLabel(parent, valueWidgetName, NULL, 0);
	XtManageChild(label1);
	XtVaSetValues(label1, XmNlabelString, valueLabel, NULL);
	XtVaSetValues(label1, XtVaTypedArg, XmNbackground, XmRString, valueBackground, strlen(valueBackground) + 1, NULL);
}

char *iconvToNewA(const char * const source, const char *sourceCharset, const char *targetCharset) {
	const int wideChar = !strcmp(sourceCharset, ICONV_WCHAR_T);

	/*
	 * bytesPerChar only makes sense when converting to UTF-8.
	 */
	const size_t sourceLength = wideChar ? wcslen((wchar_t *) source) : strlen(source); // w/o trailing  \0
	const size_t targetLength = sourceLength * (wideChar || strcmp(sourceCharset, targetCharset)
			? bytesPerChar
			: 1); // w/o trailing \0

	char * const target = malloc(targetLength + 1);

#if defined(__sun) && defined(__SVR4)
	/*
	 * On Solaris, iconv_open() fails if the source and target charsets are
	 * the same, so using memcpy() instead.
	 */
	if (!wideChar && !strcmp(sourceCharset, targetCharset)) {
		/*
		 * Conversion to the same charset.
		 */
		return memcpy(target, source, sourceLength);
	}
#endif

	errno = 0;
	const iconv_t descriptor = iconv_open(targetCharset, sourceCharset);
	if (descriptor == (iconv_t) -1) {
#if defined(__sun) && defined(__SVR4)
		/*
		 * Solaris can't convert from a single-byte charset to another
		 * single-byte charset, so UTF-8 must be used for intermediate
		 * conversion.
		 */
		if (errno == EINVAL && strcmp(targetCharset, "UTF-8")) {
			char * const utf8String = iconvToNewA(source, sourceCharset, "UTF-8");
			char * const s = iconvToNewA(utf8String, "UTF-8", targetCharset);
			free(utf8String);

			return s;
		}
#endif

		fprintf(stderr, "iconv_open(\"%s\", \"%s\") failed: errno = %d\n", targetCharset, sourceCharset, errno);
		perror("iconv_open() failed");
		return NULL;
	}

#if defined(__sun) && defined(__SVR4)
	const char *inBuf = source;
#else
	char *inBuf = (char *) source;
#endif
	char *outBuf = target;
	size_t inBytesLeft = (sourceLength + 1) * (wideChar ? sizeof(wchar_t) : sizeof(char));
	size_t outBytesLeft = targetLength + 1;

	errno = 0;
	if (iconv(descriptor, &inBuf, &inBytesLeft, &outBuf, &outBytesLeft) == (size_t) -1) {
		fprintf(stderr, "iconv() failed: errno = %d\n", errno);
		perror("iconv() failed");
		return NULL;
	}

	errno = 0;
	if (iconv_close(descriptor) == -1) {
		fprintf(stderr, "iconv_close() failed: errno = %d\n", errno);
		perror("iconv_close() failed");
		return NULL;
	}

	return target;
}

char *iconvToNewW(const wchar_t * const source, const char *targetCharset) {
	return iconvToNewA((char *) source, ICONV_WCHAR_T, targetCharset);
}

