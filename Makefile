CFLAGS = -std=c99 -O0 -g3 -pedantic -Wall -Wextra -Wconversion
LDLIBS = -lXm -lXt -lX11

ifdef COMSPEC
EXESUFFIX = .exe
LDLIBS := $(LDLIBS) -liconv
endif

.PHONY: all
all: test-motif-xmstringcreatelocalized$(EXESUFFIX)

test-motif-xmstringcreatelocalized$(EXESUFFIX): test-motif-xmstringcreatelocalized.o

.PHONY: clean
clean:
	$(RM) *.o test-motif-xmstringcreatelocalized$(EXESUFFIX)
